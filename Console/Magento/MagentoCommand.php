<?php
namespace Euwishes\Cli\Console\Magento;

use Euwishes\Cli\Console\Common\NameProvider;
use Euwishes\Cli\Console\Docker\CommandExecutor;
use Euwishes\Cli\Console\Docker\DockerExecutor;
use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\Magento\MagentoExecutor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportDatabaseCommand import the latest database from the Cloud Api.
 *
 * @package Euwishes\Cli\Console\Api
 */
class MagentoCommand extends Command
{
    const COMMAND_NAME = 'docker';

    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var \Euwishes\Cli\Console\Docker\CommandExecutor
     */
    private $commandExecutor;

    /**
     * @var MagentoExecutor
     */
    private $magentoExecutor;

    /**
     * Constructor.
     *
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @throws \LogicException When the command name is empty
     *
     * @Api
     */
    public function __construct($name = null)
    {
        parent::__construct($name);

        $environment = new Environment();
        $nameProvider = new NameProvider($environment->getProjectDirectory());
        $dockerExecutor = new DockerExecutor();

        $this->commandExecutor = new CommandExecutor($nameProvider, $dockerExecutor);
        $this->magentoExecutor = new MagentoExecutor($nameProvider, $dockerExecutor);
        $this->environment = $environment;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Execute the magento command inside docker container')
            ->setHelp('<info>' . self::COMMAND_NAME . '</info> imports the latest database from the Api.')
            ->setDefinition(
                array(
                    new InputArgument(
                        "argument1",
                        InputArgument::OPTIONAL,
                        'The arguments for magerun'
                    ),
                    new InputArgument(
                        "argument2",
                        InputArgument::OPTIONAL,
                        'The arguments for magerun'
                    ),
                    new InputArgument(
                        "argument3",
                        InputArgument::OPTIONAL,
                        'The arguments for magerun'
                    )
                )
            );
    }

    /**
     * Import the data for the given project and data type
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $arguments = $input->getArguments();

        // get rid of the first argument; its the command itself
        array_shift($arguments);
        $result = $this->magentoExecutor->execute(implode(" ", $arguments));

        $output->writeln($result->getStdOut());
        $output->writeln($result->getStdErr());
        if (!$result->wasSuccessful()) {
            $output->writeln("The command failed.");

            return 1;
        }

        return 0;
    }
}
