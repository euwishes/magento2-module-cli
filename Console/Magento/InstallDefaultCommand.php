<?php
namespace Euwishes\Cli\Console\Magento;

use Euwishes\Cli\Console\Common\NameProvider;
use Euwishes\Cli\Console\Common\TerminalModeDetection;
use Euwishes\Cli\Console\Docker\CommandExecutor;
use Euwishes\Cli\Console\Docker\DockerExecutor;
use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\Magento\MagentoExecutor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

/**
 * Class ImportDatabaseCommand import the latest database from the Cloud Api.
 *
 * @package Euwishes\Cli\Console\Api
 */
class InstallDefaultCommand extends Command
{
    const COMMAND_NAME = 'magento:install:default';

    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var \Euwishes\Cli\Console\Docker\CommandExecutor
     */
    private $commandExecutor;

    /**
     * @var MagentoExecutor
     */
    private $magentoExecutor;

    private $terminalModeDetection;

    /**
     * Constructor.
     *
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @throws \LogicException When the command name is empty
     *
     * @Api
     */
    public function __construct($name = null)
    {
        parent::__construct($name);

        $environment = new Environment();
        $nameProvider = new NameProvider($environment->getProjectDirectory());
        $dockerExecutor = new DockerExecutor();

        $this->terminalModeDetection = new TerminalModeDetection();

        $this->commandExecutor = new CommandExecutor($nameProvider, $dockerExecutor);
        $this->magentoExecutor = new MagentoExecutor($nameProvider, $dockerExecutor);
        $this->environment = $environment;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('TODO')
            ->setHelp('<info>' . self::COMMAND_NAME . '</info> todo.');
    }

    /**
     * Import the data for the given project and data type
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$config = $this->projectConfig->get();

        $result = $this->magentoExecutor->execute('setup:install --db-host=mysql --db-name=magento --db-user=root --backend-frontname=management --db-password=pw --base-url="http://dev.eu-wishes.eu" --admin-user=management --admin-password=test123 --admin-email=info@eu-wishes.eu --admin-firstname=Admin --admin-lastname=Admin --cleanup-database --use-sample-data');
        $output->writeln($result->getStdOut());
        $output->writeln($result->getStdErr());
        if (!$result->wasSuccessful()) {
            $output->writeln("The setup failed.");

            return 1;
        }

        $output->writeln("Set setup run is complete.");

        return 0;
    }
}
