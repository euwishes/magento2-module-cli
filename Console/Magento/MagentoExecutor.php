<?php
namespace Euwishes\Cli\Console\Magento;

use Euwishes\Cli\Console;
use Euwishes\Cli\Console\Common;
use Euwishes\Cli\Console\Common\CommandResult;
use Euwishes\Cli\Console\Common\NameProvider;
use Euwishes\Cli\Console\Docker;

/**
 * Class MagentoExecutor executes bin/magento commands within the PHP container.
 *
 * @package Euwishes\Cli\Console\Magento
 */
class MagentoExecutor
{
    /** @var Docker\ExecutorInterface $dockerExecutor A docker executor for running commands in docker containers */
    private $dockerExecutor;

    /** @var NameProvider $nameProvider */
    private $nameProvider;

    /**
     * Creates a new instance of the MagentoExecutor class.
     *
     * @param NameProvider $nameProvider
     * @param Docker\ExecutorInterface $dockerExecutor
     *
     */
    public function __construct(NameProvider $nameProvider, Docker\ExecutorInterface $dockerExecutor)
    {
        if (is_null($nameProvider)) {
            throw new \InvalidArgumentException("The supplied name provider cannot be null.");
        }

        if (is_null($dockerExecutor)) {
            throw new \InvalidArgumentException("Docker cannot be null.");
        }

        $this->nameProvider = $nameProvider;
        $this->dockerExecutor = $dockerExecutor;
    }

    /**
     * Execute magerun in the magento instance with the supplied commandline arguments.
     *
     * @param string $commandlineArguments
     *
     * @return CommandResult
     */
    public function execute($commandlineArguments)
    {
        $containerName = $this->nameProvider->getContainerName("php");
        $stdOut = "";
        $stdErr = "";

        try {
            $wasSuccessful = $this->executeMagento($containerName, $commandlineArguments, $stdOut, $stdErr);

            return new CommandResult($stdOut, $stdErr, $wasSuccessful);
        } catch (\Exception $executeException) {
            return new CommandResult($stdOut, $stdErr, false);
        }
    }

    /**
     * Execute magerun with the supplied arguments.
     *
     * @param string $containerName        The container name to run the command in
     * @param string $commandlineArguments The arguments for magerun (e.g. "db:import")
     *
     * @param string $stdOut               StdOut (Optional)
     * @param string $stdErr               StdErr (Optional)
     *
     * @return bool true if the command has been successfully executed; otherwise false.
     */
    private function executeMagento($containerName, $commandlineArguments, &$stdOut = null, &$stdErr = null)
    {
        $magerunCommand = "web/bin/magento " . $commandlineArguments;
        return $this->dockerExecutor->execute($containerName, $magerunCommand, $stdOut, $stdErr, true);
    }
}