<?php
namespace Euwishes\Cli\Console\Api\Credentials;

/**
 * Class Credentials contains Api credentials
 *
 * @package Euwishes\Cli\Console\Api
 */
class Credentials
{
    public $url;
    public $username;
    public $password;

    /**
     * @param string      $url      The Api URL (e.g. "https://dumps.eu-wishes.eu/")
     * @param string|null $username The username (optional)
     * @param string|null $password The password (optional)
     *
     * @throws \InvalidArgumentException If the supplied $url is null or empty
     * @throws \InvalidArgumentException If a $username was supplied but no $password
     * @throws \InvalidArgumentException If a $password was supplied but no $username
     */
    public function __construct($url, $username = null, $password = null)
    {
        if (empty($url)) {
            throw new \InvalidArgumentException("The supplied URL cannot be null or empty");
        }

        if (empty($password) && !empty($username)) {
            throw new \InvalidArgumentException("You specified a username but no password.");
        }

        if (empty($username) && !empty($password)) {
            throw new \InvalidArgumentException("You specified a password but no username.");
        }

        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Get the Api URL (e.g. "https://dumps.eu-wishes.eu/")
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the username
     *
     * @return null|string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the password
     *
     * @return null|string
     */
    public function getPassword()
    {
        return $this->password;
    }
}