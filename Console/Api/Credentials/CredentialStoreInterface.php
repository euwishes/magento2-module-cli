<?php
namespace Euwishes\Cli\Console\Api\Credentials;

/**
 * Interface CredentialStoreInterface reads and writes Api credentials
 *
 * @package Euwishes\Cli\Console\Api
 */
interface CredentialStoreInterface
{
    /**
     * Save the specified credentials
     *
     * @param Credentials $credentials
     */
    public function save(Credentials $credentials);

    /**
     * Load the saved credentials
     *
     * @return Credentials|null
     */
    public function load();

    /**
     * Delete the stored credentials
     */
    public function delete();
}