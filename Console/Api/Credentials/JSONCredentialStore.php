<?php
namespace Euwishes\Cli\Console\Api\Credentials;

/**
 * Class JSONCredentialStore read and writes Api credentials as JSON
 *
 * @package Euwishes\Cli\Console\Api
 */
class JSONCredentialStore implements CredentialStoreInterface
{
    /**
     * @var string
     */
    private $credentialStoreFilePath;

    /**
     * Creates a new instance of the JSONCredentialStore class
     *
     * @param string $credentialStoreFilePath The path to the credential store file (e.g.
     *                                        /home/user/.Api/credentials.json)
     */
    public function __construct($credentialStoreFilePath)
    {
        $this->credentialStoreFilePath = $credentialStoreFilePath;
    }

    /**
     * Save the specified credentials
     *
     * @param Credentials $credentials A credentials model
     *
     * @throws \Exception If the supplied model cannot be encoded to JSON
     * @throws \Exception If the JSON cannot be written to disc
     */
    public function save(Credentials $credentials)
    {
        // encode the json
        try {
            $json = json_encode($credentials, JSON_PRETTY_PRINT);
        } catch (\Exception $encodeException) {
            throw new \Exception(
                "Unable to encode the supplied credential model to JSON.\n↪ " . $encodeException->getMessage()
            );
        }

        // create the directory structure if necessary
        $directory = dirname($this->credentialStoreFilePath);
        try {
            if (!file_exists($directory)) {
                mkdir($directory, 0700, true);
            }
        } catch (\Exception $createDirectoryException) {
            throw new \Exception(
                "Unable to create the directory '$directory'.\n↪ " . $createDirectoryException->getMessage()
            );
        }

        // write the file
        try {
            file_put_contents($this->credentialStoreFilePath, $json);
        } catch (\Exception $writeException) {
            throw new \Exception(
                "Unable to save JSON to file $($this->credentialStoreFilePath).\n↪ " . $writeException->getMessage()
            );
        }
    }

    /**
     * Load the saved credentials
     *
     * @return Credentials|null A credentials model
     * @throws \Exception If the credential file cannot be read
     * @throws \Exception If the JSON cannot be decoded
     * @throws \Exception If the model is missing data
     */
    public function load()
    {
        // read the file
        try {
            $json = file_get_contents($this->credentialStoreFilePath);
        } catch (\Exception $readException) {
            throw new \Exception(
                "Unable to read credential file '" . $this->credentialStoreFilePath . "'.\n↪ " . $readException->getMessage()
            );
        }

        // decode the JSON
        $model = json_decode($json, true);

        // validate the model
        if (is_null($model)) {
            throw new \Exception("The credential model is empty or the JSON is invalid.");
        }

        if (isset($model["url"]) == false) {
            throw new \Exception("The credentials JSON does not contain a 'url' attribute.");
        }

        $url = $model["url"];
        $username = null;
        $password = null;

        if (isset($model["username"]) && $model["password"]) {
            $username = $model["username"];
            $password = $model["password"];
        }

        return new Credentials($url, $username, $password);

    }

    /**
     * Delete the stored credentials
     *
     * @throws \Exception If the deletion failed
     */
    public function delete()
    {
        if (!file_exists($this->credentialStoreFilePath)) {
            return;
        }

        try {
            unlink($this->credentialStoreFilePath);
        } catch (\Exception $deleteException) {
            throw new \Exception("Unable to delete file $($this->credentialStoreFilePath)");
        }
    }
}