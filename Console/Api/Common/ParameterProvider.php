<?php
namespace Euwishes\Cli\Console\Api\Common;

/**
 * Class ParameterProvider contains configuration values for the Api namespace
 *
 * @package Euwishes\Cli\Console\Api
 */
class ParameterProvider
{
    private $credentialStoreFolder = '.euwishes-api';
    private $credentialStorePath = 'credentials.json';

    /**
     * Get the credential store path in the users' home directory
     */
    public function getCredentialStorePath()
    {
        $home = $_SERVER['HOME'];
        return join(DIRECTORY_SEPARATOR, [$home, $this->credentialStoreFolder, $this->credentialStorePath]);
    }
}