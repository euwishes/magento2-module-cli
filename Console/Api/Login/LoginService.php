<?php
namespace Euwishes\Cli\Console\Api\Login;

use Euwishes\Api\Endpoint\ApiEndpointFactory;
use Euwishes\Cli\Console\Api\Credentials\Credentials;
use Euwishes\Cli\Console\Api\Credentials\CredentialStoreInterface;

/**
 * Class LoginService
 *
 * @package Euwishes\Cli\Console\Api
 */
class LoginService
{
    /**
     * @var CredentialStoreInterface
     */
    private $credentialStore;
    /**
     * @var ApiEndpointFactory
     */
    private $ApiEndpointFactory;

    /**
     * Creates a new LoginService instance.
     *
     * @param CredentialStoreInterface $credentialStore
     * @param ApiEndpointFactory $ApiEndpointFactory A Api factory
     */
    public function __construct(CredentialStoreInterface $credentialStore, ApiEndpointFactory $ApiEndpointFactory)
    {
        $this->credentialStore = $credentialStore;
        $this->ApiEndpointFactory = $ApiEndpointFactory;
    }

    /**
     * Login using the given parameters
     *
     * @param Credentials $credentials A credentials model
     *
     * @throws \InvalidArgumentException If no credentials are given
     * @throws \Exception If the connection could not be established
     * @throws \Exception If the credentials could not be saved.
     */
    public function login(Credentials $credentials)
    {
        if (is_null($credentials)) {
            throw new \InvalidArgumentException("No credentials given");
        }

        // test the credentials
        $url = $credentials->getUrl();
        $username = $credentials->getUsername();
        $ApiInstance = $this->ApiEndpointFactory->getApiInstance(
            $url,
            $username,
            $credentials->getPassword()
        );

        if ($ApiInstance->canAccess() == false) {
            throw new \Exception("The Api connection could not be established (URL: $url, Username: $username).");
        }

        // save credentials to disc
        try {
            $this->credentialStore->save($credentials);
        } catch (\Exception $saveCredentialsException) {
            throw new \Exception(
                "Failed to save credentials.\n↪ " . $saveCredentialsException->getMessage()
            );
        }
    }

    /**
     * Log out the current user.
     *
     * @throws \Exception If the log out fails
     */
    public function logout()
    {
        try {
            $this->credentialStore->delete();
        } catch (\Exception $saveCredentialsException) {
            throw new \Exception(
                "Logout failed.\n↪ " . $saveCredentialsException->getMessage()
            );
        }
    }
}