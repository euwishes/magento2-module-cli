<?php
namespace Euwishes\Cli\Console\Api\Login;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LogoutCommand logs the user out of the current Api instance.
 *
 * @package Euwishes\Cli\Console\Api
 */
class LogoutCommand extends Command
{
    const COMMAND_NAME = 'api:logout';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Log out of the current Api instance')
            ->setHelp('<info>' . self::COMMAND_NAME . '</info> logs you out of the current Api instance.');
    }

    /**
     * Save the supplied credentials to disc
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $loginServiceFactory = new LoginServiceFactory();
            $loginService = $loginServiceFactory->getLoginService();
            $loginService->logout();
        } catch (\Exception $logoutException) {
            $output->writeln(
                "Logout failed.\n↪ " . $logoutException->getMessage()
            );

            return 1;
        }

        $output->writeln("Logout succeeded.");

        return 0;
    }
}
