<?php
namespace Euwishes\Cli\Console\Api\Login;

use Euwishes\Cli\Console\Api\Credentials\Credentials;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class LoginCommand authenticates a user against a given Api instance.
 *
 * @package Euwishes\Cli\Console\Api
 */
class LoginCommand extends Command
{
    const COMMAND_NAME = 'api:login';

    const ARGUMENT_URL = 'url';
    const ARGUMENT_USERNAME = 'username';
    const ARGUMENT_PASSWORD = 'password';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Sign into a given api instance')
            ->addArgument(
                self::ARGUMENT_URL,
                InputArgument::OPTIONAL,
                "The Api URL (e.g. https://api.euwishes.io)"
            )
            ->addArgument(self::ARGUMENT_USERNAME, InputArgument::OPTIONAL, "The username")
            ->addArgument(self::ARGUMENT_PASSWORD, InputArgument::OPTIONAL, "The password")
            ->setHelp('<info>' . self::COMMAND_NAME . '</info> authenticates you against a given Api instance.');
    }

    /**
     * Use an interactive dialog to collect the command arguments if none are specified
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        // only ask for the parameters if they are not specified
        if (empty($input->getArgument(self::ARGUMENT_URL)) == false) {
            return;
        }

        /** @var QuestionHelper $questionHelper */
        $questionHelper = $this->getHelper('question');

        $ApiURLQuestion = new Question("Api URL (required): ");
        $input->setArgument(self::ARGUMENT_URL, $questionHelper->ask($input, $output, $ApiURLQuestion));

        $usernameQuestion = new Question("Username (optional): ");
        $input->setArgument(self::ARGUMENT_USERNAME, $questionHelper->ask($input, $output, $usernameQuestion));

        $passwordQuestion = new Question("Password (optional): ");
        $passwordQuestion->setHidden(true);
        $input->setArgument(self::ARGUMENT_PASSWORD, $questionHelper->ask($input, $output, $passwordQuestion));
    }

    /**
     * Save the supplied credentials to disc
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // URL
        $url = $input->getArgument(self::ARGUMENT_URL);
        if (empty($url)) {
            $output->writeln("No URL specified for the Api");

            return 1;
        }

        // Username + Password
        $username = $input->getArgument(self::ARGUMENT_USERNAME);
        $password = $input->getArgument(self::ARGUMENT_PASSWORD);

        // Validate Username + Password
        if (empty($username) && !empty($password)) {
            $output->writeln("You did specify a password, but no username.");

            return 1;
        }

        if (!empty($username) && empty($password)) {
            $output->writeln("You did specify a username, but no password.");

            return 1;
        }

        try {
            $loginServiceFactory = new LoginServiceFactory();
            $loginService = $loginServiceFactory->getLoginService();
            $credentials = new Credentials($url, $username, $password);
            $loginService->login($credentials);
        } catch (\Exception $saveCredentialsException) {
            $output->writeln(
                "Login failed.\n↪ " . $saveCredentialsException->getMessage()
            );

            return 1;
        }

        $output->writeln("Login succeeded.");

        return 0;
    }
}
