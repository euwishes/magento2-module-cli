<?php
namespace Euwishes\Cli\Console\Api\Login;

use Euwishes\Api\Endpoint\ApiEndpointFactory;
use Euwishes\Cli\Console\Api\Common\ParameterProvider;
use Euwishes\Cli\Console\Api\Credentials\JSONCredentialStore;

/**
 * Class LoginServiceFactory creates LoginService instances.
 *
 * @package Euwishes\Cli\Console\Api
 */
class LoginServiceFactory
{
    /**
     * Get a new LoginService instance.
     *
     * @return LoginService
     */
    public function getLoginService()
    {
        $parameterProvider = new ParameterProvider();
        $targetPath = $parameterProvider->getCredentialStorePath();
        $credentialStore = new JSONCredentialStore($targetPath);
        $ApiEndpointFactory = new ApiEndpointFactory();

        return new LoginService($credentialStore, $ApiEndpointFactory);
    }
}