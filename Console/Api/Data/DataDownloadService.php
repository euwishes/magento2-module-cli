<?php
namespace Euwishes\Cli\Console\Api\Data;

use Euwishes\Api\Endpoint\ApiEndpointInterface;

/**
 * Class DataDownloadService downloads project specific data such as media and databases
 *
 * @package Euwishes\Cli\Console\Api
 */
class DataDownloadService
{
    /** @var ApiEndpointInterface $ApiClient An instance of the Cloud Api client */
    private $ApiClient;

    /**
     * Creates a new DataDownloadService instance.
     *
     * @param ApiEndpointInterface $ApiClient An instance of the Api client
     */
    public function __construct(ApiEndpointInterface $ApiClient)
    {
        $this->ApiClient = $ApiClient;
    }

    /**
     * Get the data of the given type from the specified project
     *
     * @param string $projectId The project id (e.g. "magento-demo")
     * @param string $dataType The data type (e.g. "media", "database")
     *
     * @return mixed
     * @throws \Exception If an error occurred while fetching the data
     * @throws \Exception If no data was received
     */
    public function getData($projectId, $dataType)
    {
        // download the data
        $data = null;
        try {
            $data = $this->ApiClient->getData($projectId, $dataType);
        } catch (\Exception $getDataException) {
            throw new \Exception(
                "Unable to load $dataType for project '$projectId'.\n↪ " . $getDataException->getMessage()
            );
        }

        if (empty($data)) {
            throw new \Exception("No data received");
        }

        return $data;
    }
}