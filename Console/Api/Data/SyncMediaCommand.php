<?php
namespace Euwishes\Cli\Console\Api\Data;

use Euwishes\Cli\Console\Common\NameProvider;
use Euwishes\Cli\Console\Common\TerminalModeDetection;
use Euwishes\Cli\Console\Configuration\ProjectConfig;
use Euwishes\Cli\Console\Environment;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

/**
 * Class ImportMediaCommand imports media files from the Tool server.
 *
 * @package Arvato\Magento\Project\Api\CloudAPI
 */
class SyncMediaCommand extends Command
{
    const COMMAND_NAME = 'api:sync:media';

    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var ProjectConfig
     */
    private $projectConfig;

    /**
     * Constructor.
     *
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @throws \LogicException When the command name is empty
     *
     * @api
     */
    public function __construct($name = null)
    {
        parent::__construct($name);

        $environment = new Environment();
        $nameProvider = new NameProvider($environment->getProjectDirectory());

        $this->environment = $environment;
        $this->terminalModeDetection = new TerminalModeDetection();
        $this->projectConfig = new ProjectConfig($environment);
    }

    /**
     * @inheritdoc
     *
     *  $rsaKey = '~/.ssh/id_rsa_utility_server'; $serverAndUser = 'root@tools.argento.io';
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Import the latest media files')
            ->setHelp('<info>' . self::COMMAND_NAME . '</info> rsyncs the latest media files direct from server.')
        ;
    }

    /**
     * Import the data for the given project and data type
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->projectConfig->get();

        // get project id
        if (empty($config)) {
            $output->writeln(
                "Unable to determine project name. Project config could not be read."
            );

            return 1;
        }

        if (isset($config["name"]) == false) {
            $output->writeln(
                "Unable to determine project name from project config."
            );

            return 1;
        }
        /*
         * 'server' => 'www.eu-wishes.eu',
        'key' => '~/.ssh/id_rsa',
        'path' => '/var/data/prod/euwishes/media'
         */

        if (empty($config["sync"]["server"])) {
            $output->writeln(
                "No path to media for sync specified"
            );

            return 1;
        }

        if (empty($config["sync"]["key"]))
        {
            $output->writeln("The supplied rsa key path cannot be null or empty.");
            return 1;
        }

        $output->writeln(
            "Sync to path: ". $config["sync"]["localPath"]
        );

        try {
            // rsync -aLvze "ssh -i '$rsaKey'" --progress --delete --force root@tools.argento.io:/data/asus/media $targetPath
            $rsyncProcess = new Process("rsync -aLvze \"ssh -p" . $config["sync"]["port"] . " -i '" . $config["sync"]["key"] . "'\" ".
                "--progress --delete --force " . $config["sync"]["server"] . ":" . $config["sync"]["remotePath"] .
                " " . $config["sync"]["localPath"]
            );

            if ($this->terminalModeDetection->ttyIsEnabled())
            {
                $rsyncProcess->setTty(true);
            }

            $rsyncProcess->run(function ($type, $buffer)
            {
                if (Process::ERR === $type)
                {
                    echo $buffer;
                }
                else
                {
                    echo $buffer;
                }
            });


        } catch (\Exception $downloadException) {
            $output->writeln(
                "Unable to rsync data for project '" . $config["name"] . "' to " .
                $config["sync"]["localPath"] . ".\n↪ " . $downloadException->getMessage()
            );

            return 1;
        }

        $output->writeln("Media file rsync complete.");

        return 0;
    }
}
