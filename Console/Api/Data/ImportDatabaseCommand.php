<?php
namespace Euwishes\Cli\Console\Api\Data;

use Euwishes\Cli\Console\Common\NameProvider;
use Euwishes\Cli\Console\Configuration\ProjectConfig;
use Euwishes\Cli\Console\Database\DatabaseHelper;
use Euwishes\Cli\Console\Docker\CommandExecutor;
use Euwishes\Cli\Console\Docker\DockerExecutor;
use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\Magento\MagentoExecutor;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Setup\BackupRollback;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ImportDatabaseCommand import the latest database from the Cloud Api.
 *
 * @package Euwishes\Cli\Console\Api
 */
class ImportDatabaseCommand extends Command
{
    const COMMAND_NAME = 'api:import:database';

    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var ProjectConfig
     */
    private $projectConfig;

    private $databaseConnection;

    /**
     * Filesystem Directory List
     *
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var DownloaderFactory
     */
    private $downloaderFactory;

    /**
     * @var \Euwishes\Cli\Console\Docker\CommandExecutor
     */
    private $commandExecutor;

    /**
     * @var MagentoExecutor
     */
    private $magentoExecutor;

    /**
     * Constructor.
     *
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     *
     * @throws \LogicException When the command name is empty
     *
     * @Api
     */
    public function __construct(DirectoryList $directoryList)
    {
        parent::__construct();

        $environment = new Environment();
        $nameProvider = new NameProvider($environment->getProjectDirectory());
        $dockerExecutor = new DockerExecutor();

        $this->downloaderFactory = new DownloaderFactory();
        $this->commandExecutor = new CommandExecutor($nameProvider, $dockerExecutor);
        $this->magentoExecutor = new MagentoExecutor($nameProvider, $dockerExecutor);
        $this->environment = $environment;
        $this->projectConfig = new ProjectConfig($environment);
        $config = $this->projectConfig->get();
        $this->databaseConnection = new DatabaseHelper($config);
        $this->directoryList = $directoryList;
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Import the latest database')
            ->setHelp('<info>' . self::COMMAND_NAME . '</info> imports the latest database from the Api.');
    }

    /**
     * Import the data for the given project and data type
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // download the data -> /data/databasename/database
        $backupFolder = "var/backups";
        $dataType = "database";
        $databaseFilename = time() . "_db";
        $databaseArchiveName = "$databaseFilename.gz";
        $targetPath = "$backupFolder/$databaseArchiveName";

        $config = $this->projectConfig->get();
        $projectDatabase = $config['database'];

        try {
            $downloader = $this->downloaderFactory->getDownloader();
            $downloader->saveDataToPath($projectDatabase, $dataType, $targetPath);
        } catch (\Exception $downloadException) {
            $output->writeln(
                "Unable download $dataType data for project '$projectDatabase' to $targetPath.\n↪ " . $downloadException->getMessage()
            );

            return 1;
        }


        // strip the new database
        $stripDBResult = $this->commandExecutor->executeCommandInContainer('php', 'sed -i "/\b\(log_visitor_info\)\b/d" /var/www/html/web/' .$targetPath);
        $output->writeln($stripDBResult->getStdOut());
        $output->writeln($stripDBResult->getStdErr());
        if (!$stripDBResult->wasSuccessful()) {
            $output->writeln("The database strip failed.");

            return 1;
        }

        // import the new database
        $importDBResult = $this->magentoExecutor->execute("setup:rollback --no-interaction --db-file $databaseArchiveName");
        $output->writeln($importDBResult->getStdOut());
        $output->writeln($importDBResult->getStdErr());
        if (!$importDBResult->wasSuccessful()) {
            $output->writeln("The database import failed.");

            return 1;
        }

        // cleanup
        $cleanupResult = $this->commandExecutor->executeCommandInContainer("php", "rm -v web/$targetPath");
        $output->writeln($cleanupResult->getStdOut());
        $output->writeln($cleanupResult->getStdErr());
        if (!$cleanupResult->wasSuccessful()) {
            $output->writeln("Cleanup failed.");

            return 1;
        }

        // reindex
        $reindexResult = $this->magentoExecutor->execute("indexer:reindex");
        $output->writeln($reindexResult->getStdOut());
        $output->writeln($reindexResult->getStdErr());
        if (!$reindexResult->wasSuccessful()) {
            $output->writeln("The reindex failed.");

            return 1;
        }

        // clear cache
        $cacheClearResult = $this->magentoExecutor->execute("cache:clean");
        $output->writeln($cacheClearResult->getStdOut());
        $output->writeln($cacheClearResult->getStdErr());
        if (!$cacheClearResult->wasSuccessful()) {
            $output->writeln("The cache clean failed.");

            return 1;
        }

        // clear cache
        $cacheClearResult = $this->magentoExecutor->execute("setup:upgrade");
        $output->writeln($cacheClearResult->getStdOut());
        $output->writeln($cacheClearResult->getStdErr());
        if (!$cacheClearResult->wasSuccessful()) {
            $output->writeln("The cache clean failed.");

            return 1;
        }

        // clear cache
        $cacheClearResult = $this->magentoExecutor->execute("cache:clean");
        $output->writeln($cacheClearResult->getStdOut());
        $output->writeln($cacheClearResult->getStdErr());
        if (!$cacheClearResult->wasSuccessful()) {
            $output->writeln("The cache clean failed.");

            return 1;
        }

        $output->writeln("Database import complete.");

        return 0;
    }
}
