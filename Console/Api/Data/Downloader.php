<?php
namespace Euwishes\Cli\Console\Api\Data;

/**
 * Class Downloader downloads data and stores it at the given path
 *
 * @package Euwishes\Cli\Console\Api
 */
class Downloader
{
    /** @var DataDownloadService A download-service instance */
    private $dataDownloadService;

    /**
     * Creates a new instance of the DataDownloadService
     *
     * @param DataDownloadService $dataDownloadService A download-service instance
     */
    public function __construct(DataDownloadService $dataDownloadService)
    {
        $this->dataDownloadService = $dataDownloadService;
    }

    /**
     * Save the data to the given target path
     *
     * @param string $projectId  The project id (e.g. "magento-demo")
     * @param string $dataType   The data type (e.g. "media", "database")
     * @param string $targetPath The target path for the download (e.g. "media.tar.gz")
     *
     * @throws \Exception
     */
    public function saveDataToPath($projectId, $dataType, $targetPath)
    {
        // download the data
        $data = null;
        try {
            $data = $this->dataDownloadService->getData($projectId, $dataType);
        } catch (\Exception $getDataException) {
            throw new \Exception(
                "Unable to download $dataType data for '$projectId'.\n↪ ".$getDataException->getMessage()
            );
        }

        // save the data to disc
        try {
            file_put_contents($targetPath, $data);
        } catch (\Exception $saveException) {
            throw new \Exception("Error while saving data to path $targetPath.\n↪ ".$saveException->getMessage());
        }
    }
}