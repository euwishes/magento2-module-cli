<?php
namespace Euwishes\Cli\Console\Api\Data;

use Euwishes\Api\Endpoint\ApiEndpointFactory;
use Euwishes\Cli\Console\Api\Common\ParameterProvider;
use Euwishes\Cli\Console\Api\Credentials\JSONCredentialStore;

/**
 * Class DownloaderFactory creates Downloader instances
 *
 * @package Euwishes\Cli\Console\Api
 */
class DownloaderFactory
{
    /**
     * Get a new downloader instance
     *
     * @return Downloader
     * @throws \Exception If the credentials cannot be loaded
     */
    public function getDownloader()
    {
        // load credentials
        $credentials = null;
        $parameterProvider = new ParameterProvider();
        $targetPath = $parameterProvider->getCredentialStorePath();
        try {
            $credentialStore = new JSONCredentialStore($targetPath);
            $credentials = $credentialStore->load();
        } catch (\Exception $loadCredentialsException) {
            throw new \Exception(
                "Unable to load credentials from $targetPath.\n↪ " . $loadCredentialsException->getMessage()
            );
        }

        // create Api instance
        $ApiEndpointFactory = new ApiEndpointFactory();
        $ApiEndpoint = $ApiEndpointFactory->getApiInstance(
            $credentials->getUrl(),
            $credentials->getUsername(),
            $credentials->getPassword());

        // create the download service
        $downloadService = new DataDownloadService($ApiEndpoint);
        $downloader = new Downloader($downloadService);

        return $downloader;
    }
}