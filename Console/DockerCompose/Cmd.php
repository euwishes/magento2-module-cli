<?php
namespace Euwishes\Cli\Console\DockerCompose;

use Euwishes\Cli\Console;
use Euwishes\Cli\Console\Common;
use Symfony\Component\Process\Process;
use Euwishes\Cli\Console\Docker;
/**
 * Class MagerunExecutor executes docker-compose in the current project.
 * @package Euwishes\Cli\Console\DockerCompose
 */
class Cmd
{
    /** @var string $workingDirectory The base directory */
    private $workingDirectory;

    /** @var ConfigFileProvider $configFileProvider Configuration file provider */
    private $configFileProvider;

    /** @var Common\TerminalModeDetection $terminalModeDetection */
    private $terminalModeDetection;

    /**
     * Creates a new instance of the DockerComposeInfrastructure class.
     *
     * @param string $projectDirectory The path of the project directory (e.g. "/home/user/src/project").
     * @param ConfigFileProvider $configFileProvider
     */
    public function __construct($projectDirectory, ConfigFileProvider $configFileProvider)
    {
        if (empty($projectDirectory))
        {
            throw new \InvalidArgumentException("The specified project diretory path cannot be null or empty.");
        }

        if ($configFileProvider == null)
        {
            throw new \InvalidArgumentException("The supplied config file provider cannot be null.");
        }

        $this->workingDirectory = $projectDirectory;
        $this->configFileProvider = $configFileProvider;
        $this->terminalModeDetection = new Common\TerminalModeDetection();
    }

    /**
     * Execute docker-compose with the supplied commandline arguments.
     *
     * @param array $arguments The command line arguments
     * @param string $stdout Stdout (Optional)
     * @param string $stderr Stderr (Optional)
     *
     * @return bool true if the command has been successfully executed; otherwise false.
     */
    public function execute($arguments, &$stdout = null, &$stderr = null)
    {
        $fileFilename = $this->configFileProvider->getDockerComposeFileName();
        $dockerComposeProcess = new Process("docker-compose -f " . $fileFilename . " " . implode(" ", $arguments));
        $dockerComposeProcess->setWorkingDirectory($this->workingDirectory);
        $dockerComposeProcess->setTimeout(3600);

        if ($this->terminalModeDetection->ttyIsEnabled())
        {
            $dockerComposeProcess->setTty(true);
        }

        $dockerComposeProcess->run(function ($type, $buffer)
        {
            if (Process::ERR === $type)
            {
                echo $buffer;
            }
            else
            {
                echo $buffer;
            }
        });

        if (!is_null($stdout))
        {
            $stdout = $dockerComposeProcess->getOutput();
        }

        if (!is_null($stderr))
        {
            $stderr = $dockerComposeProcess->getErrorOutput();
        }

        if (!$dockerComposeProcess->isSuccessful())
        {
            return false;
        }

        return true;
    }
}
