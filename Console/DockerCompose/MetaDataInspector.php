<?php
/**
 * @category    Arvato
 * @package     magento-project-Api
 * @copyright   Copyright (c) arvato (http://arvato-hightech-ecommerce.com)
 */
namespace Euwishes\Cli\Console\DockerCompose;

use Euwishes\Cli\Console\Common;
use Euwishes\Cli\Console\Docker;

/**
 * Class MetaDataInspector provides access the docker container meta data
 * @package Euwishes\Cli\Console\DockerCompose
 */
class MetaDataInspector
{
    /** @var Common\NameProvider $nameProvider An instance of the name provider */
    private $nameProvider;

    /** @var Cmd $docker An instance of the docker command executor */
    private $docker;

    /**
     * Creates a new instance of the meta data inspector
     *
     * @param Common\NameProvider $nameProvider An instance of the name provider
     * @param Docker\Cmd $dockerCommand An instance of the docker command executor
     */
    public function __construct($nameProvider, $dockerCommand)
    {
        if ($nameProvider == null)
        {
            throw new \InvalidArgumentException("The supplied name provider cannot be null");
        }

        if ($dockerCommand == null)
        {
            throw new \InvalidArgumentException("The supplied docker command executor cannot be null");
        }

        $this->nameProvider = $nameProvider;
        $this->docker = $dockerCommand;
    }

    /**
     * Get a configuration value from the first docker container with the specified type
     *
     * @param string $containerType The container type (e.g. php, nginx, mysql)
     * @param string $key The docker config key path (e.g. .NetworkSettings.IPAddress)
     *
     * @return string
     */
    public function getConfigValue($containerType, $key)
    {
        $containerName = $this->nameProvider->getContainerName($containerType);

        $stdOut = "";
        $stdErr = "";
        $interactive = false;

        $this->docker->execute(array('inspect', '--format "{{ ' . $key . ' }}"', $containerName), $stdOut, $stdErr, $interactive);

        return trim($stdOut);
    }
}