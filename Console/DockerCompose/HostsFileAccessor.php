<?php
/**
 * @category    Arvato
 * @package     magento-project-Api
 * @copyright   Copyright (c) arvato (http://arvato-hightech-ecommerce.com)
 */
namespace Euwishes\Cli\Console\DockerCompose;

use Euwishes\Cli\Console;
use Euwishes\Cli\Console\Docker;
use Euwishes\Cli\Console\Common;

/**
 * Class HostsFileAccessor provides access to the hosts file of a container
 * @package Euwishes\Cli\Console\DockerCompose
 */
class HostsFileAccessor
{
    /** @var Common\NameProvider $nameProvider An instance of the name provider */
    private $nameProvider;

    /** @var Docker\ExecutorInterface $dockerExecutor An instance of the docker command executor */
    private $dockerExecutor;

    /**
     * Creates a new instance of the hosts file accessor
     *
     * @param Common\NameProvider $nameProvider An instance of the name provider
     * @param Docker\ExecutorInterface $dockerExecutor An instance of the docker command executor
     */
    public function __construct($nameProvider, $dockerExecutor)
    {
        if ($nameProvider == null)
        {
            throw new \InvalidArgumentException("The supplied name provider cannot be null");
        }

        if ($dockerExecutor == null)
        {
            throw new \InvalidArgumentException("The supplied docker command executor cannot be null");
        }

        $this->nameProvider = $nameProvider;
        $this->dockerExecutor = $dockerExecutor;
    }

    /**
     * Add a hosts file entry to container
     *
     * @param string $targetContainerType The type (e.g. php, nginx, mysql) of the target container
     * @param string $ip The ip address to create a hosts file entry for
     * @param string $host The hostname
     */
    public function addHostRecord($targetContainerType, $ip, $host)
    {
        $targetContainerName = $this->nameProvider->getContainerName($targetContainerType);

        $stdOut = "";
        $stdErr = "";
        $interactive = false;
        $command = 'sh -c "echo \''.$ip.' '.$host.'\' >> /etc/hosts"';

        $this->dockerExecutor->execute($targetContainerName, $command, $stdOut, $stdErr, $interactive);
    }

}
