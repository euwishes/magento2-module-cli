<?php
/**
 * @category    Arvato
 * @package     magento-project-Api
 * @copyright   Copyright (c) arvato (http://arvato-hightech-ecommerce.com)
 */
namespace Euwishes\Cli\Console\DockerCompose;

use Euwishes\Cli\Console;

/**
 * Class ConfigFileProvider provides the name and paths of the docker-compose configuration files
 * @package Euwishes\Cli\Console\DockerCompose
 */
class ConfigFileProvider
{
    /** @var string $workingDirectory The base directory */
    private $workingDirectory;
    /** @var string $environmentName The name of the current environment */
    private $environmentName;

    /**
     * Creates a new instance of the ConfigFileProvider class.
     *
     * @param Console\Environment $environment
     *
     */
    public function __construct(Console\Environment $environment)
    {
        if (empty($environment))
        {
            throw new \InvalidArgumentException("The specified environment cannot be null.");
        }

        $this->workingDirectory = $environment->getProjectDirectory();
        $this->environmentName = $environment->getEnvironmentName();
    }

    /**
     * Get the current docker-compose file name
     */
    public function getDockerComposeFileName()
    {
        $environmentName = $this->environmentName;

        // normalize the environment name
        $normalizedEnvironmentName = strtolower(trim($environmentName));

        switch ($normalizedEnvironmentName)
        {
            case Console\Environment::NAME_DEFAULT:
            case Console\Environment::NAME_DEV:
            case "":
                return "docker-compose.yml";

            default:
                return sprintf("docker-compose.%s.yml", $normalizedEnvironmentName);
        }
    }
}
