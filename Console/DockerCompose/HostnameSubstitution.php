<?php
/**
 * @category    Arvato
 * @package     magento-project-Api
 * @copyright   Copyright (c) arvato (http://arvato-hightech-ecommerce.com)
 */
namespace Euwishes\Cli\Console\DockerCompose;

use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\DockerCompose;

/**
 * Class HostnameSubstitution provides functionality for setting the hostname/server name in the docker-compose yml file
 * @package Euwishes\Cli\Console\DockerCompose
 */
class HostnameSubstitution
{
    /**
     * @var ConfigFileProvider $configFileProvider
     */
    private $configFileProvider;
    /**
     * @var Environment
     */
    private $environment;

    /**
     * Creates a new instance of the ConfigFileProvider class
     *
     * @param Environment $environment
     * @param ConfigFileProvider $configFileProvider
     */
    public function __construct(Environment $environment, DockerCompose\ConfigFileProvider $configFileProvider)
    {
        if ($configFileProvider == null)
        {
            throw new \InvalidArgumentException("The supplied config file provider cannot be null");
        }

        $this->configFileProvider = $configFileProvider;
        $this->environment = $environment;
    }

    /**
     * Substitute the hostname placeholder in the current docker-compose yml file with the current hostname
     */
    public function substituteHostname()
    {
        $currentDockerComposeFile = $this->configFileProvider->getDockerComposeFileName();
        $workingDirectory = $this->environment->getProjectDirectory();

        // read the docker-compose yml file
        $dockerComposeFilePath = $workingDirectory . DIRECTORY_SEPARATOR . $currentDockerComposeFile;
        $dockerComposeYmlContent = file_get_contents($dockerComposeFilePath);

        // replace hostnames
        $hostnamePattern = '/hostname: (.+)/';
        $replacement = "hostname: " . $this->getCurrentHostname();
        $updatedDockerComposeYmlContent = preg_replace($hostnamePattern, $replacement, $dockerComposeYmlContent);

        // abort if no changed occurred
        $noChange = $dockerComposeYmlContent == $updatedDockerComposeYmlContent;
        if ($noChange)
        {
            return;
        }

        // write updated docker-compose yml file
        $writeResult = file_put_contents($dockerComposeFilePath, $updatedDockerComposeYmlContent);
        if ($writeResult == false)
        {
            throw new \Exception("Could not update hostname in file $dockerComposeFilePath.");
        }
    }

    /**
     * Get the current hostname of the system
     * @returns string The current hostname
     */
    private function getCurrentHostname()
    {
        return gethostname();
    }
}
