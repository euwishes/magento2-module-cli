<?php
/**
 * @category    Arvato
 * @package     magento-project-Api
 * @copyright   Copyright (c) arvato (http://arvato-hightech-ecommerce.com)
 */
namespace Euwishes\Cli\Console\DockerCompose;

use Euwishes\Cli\Console;

/**
 * Class ContainerInfoProvider returns configuration details for docker containers
 * @package Euwishes\Cli\Console\DockerCompose
 */
class ContainerInfoProvider
{
    /**
     * @var MetaDataInspector
     */
    private $metaDataInspector;

    /**
     * Creates a new instance of the ContainerInfoProvider class.
     * @param MetaDataInspector $metaDataInspector An instance of the meta data inspector
     */
    public function __construct($metaDataInspector)
    {
        if ($metaDataInspector == null)
        {
            throw new \InvalidArgumentException("The supplied meta data inspector cannot be null");
        }

        $this->metaDataInspector = $metaDataInspector;
    }

    /**
     * Get the ip address of the container with the specified type
     *
     * @param string $containerType The container type (e.g. php, nginx, mysql)
     *
     * @return string The ip address of the first container
     */
    public function getContainerIpAdress($containerType)
    {
        return $this->metaDataInspector->getConfigValue($containerType, '.NetworkSettings.IPAddress');
    }

    /**
     * Get the domain name of the container with the specified type
     *
     * @param string $containerType The container type (e.g. php, nginx, mysql)
     *
     * @return string The configured host name of the container with the given type
     */
    public function getContainerDomainname($containerType)
    {
        return $this->metaDataInspector->getConfigValue($containerType, '.Config.Domainname ');
    }
}