<?php
/**

 */
namespace Euwishes\Cli\Console\Docker;

/**
 * Interface ExecutorInterface provides functionality for executing commands in docker containers.
 * @package Euwishes\Cli\Console\Magerun
 */
interface ExecutorInterface
{
    /**
     * Execute the supplied command in the given docker container
     *
     * @param string $containerName The name of the container to execute the command in
     * @param string $command The command to execute in the docker container
     *
     * @param string $stdOut StdOut (optional)
     * @param string $stdErr StdErr (optional)
     * @param bool $interactive (optional, default: true)
     *
     * @return bool true if the command has been successfully executed; otherwise false.
     */
    public function execute($containerName, $command, &$stdOut = null, &$stdErr = null, $interactive = true);
}