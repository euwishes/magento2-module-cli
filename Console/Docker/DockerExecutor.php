<?php
/**

 */
namespace Euwishes\Cli\Console\Docker;

/**
 * Class DockerExecutor executes commands in docker containers using docker exec.
 * @package Euwishes\Cli\Console\Magerun
 */
class DockerExecutor implements ExecutorInterface
{
    /** @var Cmd $dockerCommand */
    private $dockerCommand;

    public function __construct()
    {
        $this->dockerCommand = new Cmd();
    }

    /**
     * Execute the supplied command in the given docker container
     *
     * @param string $containerName The name of the container to execute the command in
     * @param string $command The command to execute in the docker container
     *
     * @param string $stdOut StdOut (optional)
     * @param string $stdErr StdErr (optional)
     * @param bool $interactive (optional, default: true)
     *
     * @return bool true if the command has been successfully executed; otherwise false.
     */
    public function execute($containerName, $command, &$stdOut = null, &$stdErr = null, $interactive = true)
    {
        $interactiveOption="";
        if ($interactive)
        {
            $interactiveOption = " -ti";
        }

        return $this->dockerCommand->execute(array("exec" . $interactiveOption, $containerName, $command), $stdOut, $stdErr);
    }
}