<?php
/**
 * @category    Arvato
 * @package     magento-project-Api
 * @copyright   Copyright (c) arvato (http://arvato-hightech-ecommerce.com)
 */
namespace Euwishes\Cli\Console\Docker;

use Symfony\Component\Process\Process;

/**
 * Class ExecutorFactory return the appropriate executor instance for the current environment
 * @package Euwishes\Cli\Console\Docker
 */
class ExecutorFactory
{
    /**
     * Get an instance of the docker executor
     */
    public function getExecutor()
    {
        if ($this->dockerEnterIsAvailable())
        {
            // use the docker-enter (nsenter) workarround for older kernels
            return new NsenterExecutor();
        }

        // use a native docker enter executor
        return new DockerExecutor();
    }

    /**
     * Check if docker-enter is available on the system.
     *
     * @return bool true if docker-enter is available on the system; otherwise false
     */
    private function dockerEnterIsAvailable()
    {
        $dockerProcess = new Process("docker-enter");
        $exitCode = $dockerProcess->run();
        $dockerEnterIsAvailable = $exitCode == 0;
        return $dockerEnterIsAvailable;
    }
}