<?php
namespace Euwishes\Cli\Console\Docker;

use Euwishes\Cli\Console\Common;
use Symfony\Component\Process\Process;

/**
 * Class NsenterExecutor executes commands in docker containers using nsenter.
 * @package Euwishes\Cli\Console\Magerun
 */
class NsenterExecutor implements ExecutorInterface
{
    /** @var Common\TerminalModeDetection $terminalModeDetection */
    private $terminalModeDetection;

    public function __construct()
    {
        $this->terminalModeDetection = new Common\TerminalModeDetection();
    }

    /**
     * Execute the supplied command in the given docker container
     *
     * @param string $containerName The name of the container to execute the command in
     * @param string $command The command to execute in the docker container
     *
     * @param string $stdOut StdOut (optional)
     * @param string $stdErr StdErr (optional)
     * @param bool $interactive (optional, default: true)
     *
     * @return bool true if the command has been successfully executed; otherwise false.
     */
    public function execute($containerName, $command, &$stdOut = null, &$stdErr = null, $interactive = true)
    {
        $dockerProcess = new Process("docker-enter $containerName $command");
        $dockerProcess->setTimeout(3600);

        if ($this->terminalModeDetection->ttyIsEnabled())
        {
            $dockerProcess->setTty(true);
        }

        $dockerProcess->run();

        if (!$dockerProcess->isSuccessful())
        {
            return false;
        }

        if (!is_null($stdOut))
        {
            $stdOut = $dockerProcess->getOutput();
        }

        if (!is_null($stdErr))
        {
            $stdErr = $dockerProcess->getErrorOutput();
        }

        return true;
    }
}