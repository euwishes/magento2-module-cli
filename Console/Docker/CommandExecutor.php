<?php
namespace Euwishes\Cli\Console\Docker;

use Euwishes\Cli\Console\Common\CommandResult;
use Euwishes\Cli\Console\Common\NameProvider;

/**
 * Class CommandExecutor executes commands in docker containers.
 * @package Euwishes\Cli\Console\Services
 */
class CommandExecutor
{
    /**
     * @var DockerExecutor
     */
    private $dockerExecutor;

    /**
     * @var NameProvider
     */
    private $nameProvider;

    /**
     * Creates a new CommandExecutor class instance.
     *
     * @param NameProvider $containerNameProvider A container name provider instance
     * @param DockerExecutor $dockerExecutor A docker executor instance
     */
    public function __construct(NameProvider $containerNameProvider, DockerExecutor $dockerExecutor)
    {
        $this->nameProvider = $containerNameProvider;
        $this->dockerExecutor = $dockerExecutor;
    }

    /**
     * Execute a command in a container with the given type
     *
     * @param string $targetContainerType The container type (e.g. "php")
     * @param string $command The command
     *
     * @return CommandResult
     */
    public function executeCommandInContainer($targetContainerType, $command)
    {
        $interactive = false;
        $stdOut = "";
        $stdErr = "";
        try {
            $targetContainerName = $this->nameProvider->getContainerName($targetContainerType);

            $wasSuccessful = $this->dockerExecutor->execute($targetContainerName, $command, $stdOut, $stdErr, $interactive);
            return new CommandResult($stdOut, $stdErr, $wasSuccessful);

        } catch (\Exception $commandException) {
            return new CommandResult($stdOut, $stdErr, false);
        }
    }
}