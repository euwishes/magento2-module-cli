<?php
/**

 */
namespace Euwishes\Cli\Console\Docker;

use Euwishes\Cli\Console\Common;
use Symfony\Component\Process\Process;

/**
 * Class MagerunExecutor executes docker.
 * @package Euwishes\Cli\Console\Magerun
 */
class Cmd
{
    /** @var Common\TerminalModeDetection $terminalModeDetection */
    private $terminalModeDetection;

    public function __construct()
    {
        $this->terminalModeDetection = new Common\TerminalModeDetection();
    }

    /**
     * Execute docker with the supplied commandline arguments.
     *
     * @param array $arguments The docker command line arguments
     * @param string $stdOut StdOut (Optional)
     * @param string $stdErr StdErr (Optional)
     *
     * @param bool $interactive
     *
     * @return bool true if the command has been successfully executed; otherwise false.
     */
    public function execute($arguments, &$stdOut = null, &$stdErr = null, $interactive = true)
    {
        $dockerProcess = new Process("docker " . implode(" ", $arguments));
        $dockerProcess->setTimeout(3600);

        if ($interactive && $this->terminalModeDetection->ttyIsEnabled())
        {
            $dockerProcess->setTty(true);
        }

        $dockerProcess->run();

        if (!$dockerProcess->isSuccessful())
        {
            return false;
        }

        if (!is_null($stdOut))
        {
            $stdOut = $dockerProcess->getOutput();
        }

        if (!is_null($stdErr))
        {
            $stdErr = $dockerProcess->getErrorOutput();
        }

        return true;
    }
}