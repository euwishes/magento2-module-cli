<?php
namespace Euwishes\Cli\Console;

/**
 * Class Environment represents the current environment (e.g. "default" vs. "int")
 *
 * @package Euwishes\Cli\Console
 */
final class Environment
{
    const ENV_PATH = "config/magento/env/";
    const DB_PATH = "config/magento/db/";
    const MAGENTO_ENV_PATH = "app/etc/env.php";

    const NAME_DEFAULT = "base";
    const NAME_DEV = "dev";
    const NAME_INT = "int";
    const NAME_PPE = "ppe";
    const NAME_PROD1 = "prod1";
    const NAME_PROD2 = "prod2";

    /** @var string - The name of current environment - is used for lazy loading */
    private $currentEnvironmentName;

    /**
     * Get the absolute path of the current project
     *
     * @return string The absolute path of the current project folder
     */
    public function getProjectDirectory()
    {
        return dirname(dirname(dirname(dirname(dirname(__FILE__)))));
    }

    /**
     * Gets the absolute path to the current package
     *
     * @return string
     */
    public function getPackageDirectory()
    {
        return dirname(dirname(__FILE__));
    }

    /**
     * Sets the environment file flag
     *
     * @param string $name the environment name e.g.: Environment::NAME_INT
     */
    public function setEnvironmentFlag($name)
    {
        $magentoRootDir = $this->getMagentoRootDirectory();

        // Remove all existing .environment files
        $directory = new \DirectoryIterator($magentoRootDir);
        $files = new \RegexIterator($directory, '/\.environment$/');
        foreach ($files as $file) {
            $fullPath = join(DIRECTORY_SEPARATOR, array($magentoRootDir, $file));
            unlink($fullPath);
        }

        // create new .environment file flag
        $fileName = join(DIRECTORY_SEPARATOR, array($magentoRootDir, $name . '.environment'));
        touch($fileName);
        chmod($fileName, 0644);

        // write also into the lazy loading environment name field
        $this->currentEnvironmentName = $name;
    }

    /**
     * Gets the name of current environment
     *
     * @return string
     */
    public function getEnvironmentName()
    {
        $magentoRootDir = $this->getMagentoRootDirectory();
        if (!file_exists($magentoRootDir)) {
            return self::NAME_DEFAULT;
        }

        // Remove all existing .environment files
        $directory = new \DirectoryIterator($magentoRootDir);
        $files = new \RegexIterator($directory, '/\.environment$/');
        foreach ($files as $file) {
            return trim(str_replace('.environment', '', $file));
        }

        return self::NAME_DEFAULT;
    }

    /**
     * Gets the magento root directory
     *
     * @return string - Magento root directory
     * @throws \Exception
     */
    public function getMagentoRootDirectory()
    {
        return join(DIRECTORY_SEPARATOR, array(self::getProjectDirectory(), "pub"));
    }
}
