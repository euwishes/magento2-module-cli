<?php
namespace Euwishes\Cli\Console\Database;

use Euwishes\Cli\Console\Common\NameProvider;
use Euwishes\Cli\Console\Configuration\ProjectConfig;
use Euwishes\Cli\Console\Docker\CommandExecutor;
use Euwishes\Cli\Console\Docker\DockerExecutor;
use Euwishes\Cli\Console\Environment;
use Magento\MediaStorage\Model\Config\Source\Storage\Media\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SetEnvironmentCommand set the environment name.
 *
 * @package Euwishes\Cli\Console\Database
 */
class QueryDatabaseCommand extends Command
{
    const SQL_QUERY_ARGUMENT_NAME = "sql-query";
    const COMMAND_NAME = 'db:query';

    /**
     * @var Environment
     */
    private $environment;

    /**
     * @var ProjectConfig
     */
    private $projectConfig;

    private $databaseConnection;

    /**
     * @var \Euwishes\Cli\Console\Docker\CommandExecutor
     */
    private $commandExecutor;

    public function __construct($name = null)
    {
        parent::__construct($name);

        $environment = new Environment();
        $nameProvider = new NameProvider($environment->getProjectDirectory());
        $dockerExecutor = new DockerExecutor();
        $this->commandExecutor = new CommandExecutor($nameProvider, $dockerExecutor);
        $this->environment = $environment;
        $this->projectConfig = new ProjectConfig($environment);
        $config = $this->projectConfig->get();
        $this->databaseConnection = new DatabaseHelper($config);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Executes an SQL query on the database defined in env.php')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> sets the current environment.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::SQL_QUERY_ARGUMENT_NAME,
                        InputArgument::OPTIONAL,
                        'SQL query'
                    ),
                )
            );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get the environment name parameter
        $query = $input->getArgument(self::SQL_QUERY_ARGUMENT_NAME);
        if (empty($query)) {
            $output->writeln("The supplied sql-query cannot be null or empty.");
            return 1;
        }

        $exec   = sprintf(
            'mysql %s -e %s',
            $this->databaseConnection->getMysqlClientToolConnectionString(),
            escapeshellarg($query)
        );

        $dbResult = $this->commandExecutor->executeCommandInContainer("mysql", $exec);
        $output->writeln($dbResult->getStdOut());
        $output->writeln($dbResult->getStdErr());
        if (!$dbResult->wasSuccessful()) {
            $output->writeln("The sql query failed.");

            return 1;
        }

        return 0;
    }
}