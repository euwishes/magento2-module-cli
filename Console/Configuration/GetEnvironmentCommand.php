<?php
namespace Euwishes\Cli\Console\Configuration;

use Euwishes\Cli\Console\Environment;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetEnvironmentCommand returns the current environment name
 *
 * @package Euwishes\Cli\Console\Magento
 */
class GetEnvironmentCommand extends Command
{
    const COMMAND_NAME = 'env:get';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Get the current environment name')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> returns the current environment name.');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $environment = new Environment();
        $environmentName = $environment->getEnvironmentName();
        $output->writeln($environmentName);
    }
}
