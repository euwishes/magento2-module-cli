<?php
namespace Euwishes\Cli\Console\Configuration;

use Euwishes\Cli\Console\Environment;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SetEnvironmentCommand set the environment name.
 *
 * @package Euwishes\Cli\Console\Configuration
 */
class SetEnvironmentCommand extends Command
{
    const ENVIRONMENT_NAME_ARGUMENT_NAME = "environment-name";
    const COMMAND_NAME = 'env:set';

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Sets the environment (e.g. "default, int, prod").')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> sets the current environment.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ENVIRONMENT_NAME_ARGUMENT_NAME,
                        InputArgument::REQUIRED,
                        'The name of the environment (e.g.: default, int, prod)'
                    ),
                )
            );
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get the environment name parameter
        $newEnvironmentName = $input->getArgument(self::ENVIRONMENT_NAME_ARGUMENT_NAME);
        if (empty($newEnvironmentName)) {
            $output->writeln("The supplied environment name cannot be null or empty.");

            return;
        }

        $environment = new Environment();

        // set the environment
        $environment->setEnvironmentFlag($newEnvironmentName);

        // deploy the project configuration
        $projectConfig = new ProjectConfig($environment);
        $projectConfig->configure();
    }
}