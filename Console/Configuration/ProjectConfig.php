<?php
namespace Euwishes\Cli\Console\Configuration;

use Euwishes\Cli\Console\Environment;

/**
 * Class ProjectConfig merges all environment configuration.
 *
 * @package Euwishes\Cli\Console\Configuration
 */
class ProjectConfig
{
    /** @var Environment $environment */
    private $environment;
    /** @var string $magentoConfigFilePath */
    private $magentoConfigFilePath;
    /** @var string $defaultConfigFilePath */
    private $defaultConfigFilePath;
    /** @var string $envConfigFilePath */
    private $envConfigFilePath;

    private $envFilePath;

    /**
     * Creates a new instance of the ProjectConfig class
     *
     * @param Environment $environment The current environment
     *
     * @throws \InvalidArgumentException If no $environment is supplied.
     */
    public function __construct(Environment $environment)
    {
        if (is_null($environment)) {
            throw new \InvalidArgumentException("No environment supplied.");
        }

        $this->environment = $environment;
        $this->magentoConfigFilePath = $this->environment->getProjectDirectory() . DIRECTORY_SEPARATOR . Environment::MAGENTO_ENV_PATH;

        $this->envFilePath = $this->environment->getProjectDirectory() . DIRECTORY_SEPARATOR . Environment::ENV_PATH;
        $this->defaultConfigFilePath = $this->envFilePath . DIRECTORY_SEPARATOR . Environment::NAME_DEFAULT . '.php';

        if (!empty($this->environment->getEnvironmentName())) {
            $this->envConfigFilePath = $this->envFilePath . DIRECTORY_SEPARATOR . $this->environment->getEnvironmentName() . '.php';
        }
    }

    /**
     * Merge config files and save them in env.php
     */
    public function configure()
    {
        $envConfig = [];
        if (!empty($this->envConfigFilePath)) {
            $envConfig = include $this->envConfigFilePath;
        }
        /** @var array $defaultConfig */
        $defaultConfig = include $this->defaultConfigFilePath;

        /** @var array $magentoConfig */
        $magentoConfig = include $this->magentoConfigFilePath;

        $mergedConfig = array_merge($magentoConfig, $defaultConfig, $envConfig);

        $val = var_export($mergedConfig, true);

        $handle = fopen($this->magentoConfigFilePath, 'w');
        fwrite($handle, "<?php return $val;");
    }

    public function get()
    {
        return include $this->magentoConfigFilePath;
    }
}