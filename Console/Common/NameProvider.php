<?php
/**
 */
namespace Euwishes\Cli\Console\Common;

/**
 * Class NameProvider has functionality for providing project and container name of docker-compose containers
 * @package Euwishes\Cli\Console\Common
 */
class NameProvider
{
    /** @var string $workingDirectory The base directory */
    private $workingDirectory;

    /**
     * Creates a new instance of the NameProvider class.
     *
     * @param string $projectDirectory The path of the project directory (e.g. "/home/user/src/project").
     *
     * @throws \InvalidArgumentException If the supplied $projectDirectory parameter is null or empty.
     */
    public function __construct($projectDirectory)
    {
        if (empty($projectDirectory))
        {
            throw new \InvalidArgumentException("The specified project diretory path cannot be null or empty.");
        }

        $this->workingDirectory = $projectDirectory;
    }

    /**
     * Get the container name based on the container type
     *
     * @param string $containerType The container type (e.g. php, nginx, ...)
     *
     * @return string The container name for the supplied container type.
     */
    public function getContainerName($containerType)
    {
        return sprintf("%s_%s_1", $this->getProjectName(), $containerType);
    }

    /**
     * Get the current docker-compose project name (based on the directory name)
     *
     * @return string
     */
    private function getProjectName()
    {
        // get the directory name
        $directoryName = basename($this->workingDirectory);

        // prepare the directory name
        $projectName = strtolower($directoryName);
        $projectName = preg_replace('/[-_\s]/', "", $projectName);

        return $projectName;
    }
}