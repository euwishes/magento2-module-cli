<?php
/**

 */
namespace Euwishes\Cli\Console\Common;

/**
 * Class TerminalModeDetection knows whether the Api is being used from a tty or not.
 * @package Euwishes\Cli\Console\Common
 */
class TerminalModeDetection
{
    /**
     * Gets a flag indicating whether the Api is used from a tty or not (default: true)
     * @return bool
     */
    public function ttyIsEnabled()
    {
        return posix_isatty(STDOUT);
    }
}