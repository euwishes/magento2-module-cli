<?php
namespace Euwishes\Cli\Console\Common;

/**
 * Class CommandResult represents the output and result of a command.
 *
 * @package Euwishes\Cli\Console\Common
 */
class CommandResult
{
    private $stdOut;
    private $stdErr;
    private $wasSuccessFul;

    public function __construct($stdOut, $stdErr, $wasSuccessFul)
    {
        $this->stdOut = $stdOut;
        $this->stdErr = $stdErr;
        $this->wasSuccessFul = $wasSuccessFul;
    }

    /**
     * Get the standard output of the command
     *
     * @return string
     */
    public function getStdOut()
    {
        return $this->stdOut;
    }

    /**
     * Get the error output of the command
     *
     * @return string
     */
    public function getStdErr()
    {
        return $this->stdErr;
    }

    /**
     * Get a flag indicating if the command execution was successful or not.
     *
     * @return bool
     */
    public function wasSuccessful()
    {
        return $this->wasSuccessFul;
    }
}