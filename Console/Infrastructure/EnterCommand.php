<?php
/**

 */
namespace Euwishes\Cli\Console\Infrastructure;

use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\DockerCompose;
use Euwishes\Cli\Console\Common;
use Euwishes\Cli\Console\Docker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class EnterCommand extends Command
{
    const COMMAND_NAME = 'enter';
    const ARGUMENTNAME_CONTAINERTYPE = "containertype";

    /** @var Docker\ExecutorInterface $dockerExecutor A docker executor for running commands in docker containers */
    private $dockerExecutor;

    /** @var Common\NameProvider $nameProvider */
    private $nameProvider;

    public function __construct()
    {
        parent::__construct();

        $environment = new Environment();
        $this->nameProvider = new Common\NameProvider($environment->getProjectDirectory());

        $executorFactory = new Docker\ExecutorFactory();
        $this->dockerExecutor = $executorFactory->getExecutor();
    }

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Enter a running container.')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> starts a bash in a running container.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENTNAME_CONTAINERTYPE,
                        InputArgument::REQUIRED,
                        'The container to start the bash in (e.g. php)'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $containerType = $input->getArgument(self::ARGUMENTNAME_CONTAINERTYPE);
        $containerName = $this->nameProvider->getContainerName($containerType);
        $this->dockerExecutor->execute($containerName, "bash");
    }
}
