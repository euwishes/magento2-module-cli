<?php
/**

 */
namespace Euwishes\Cli\Console\Infrastructure;

use Euwishes\Cli\Console\Common;
use Euwishes\Cli\Console\Docker;
use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\DockerCompose;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Parser;

/**
 * Class DockerComposeInfrastructure provides functions for controlling the project infrastructure using docker-compose.
 * @package Euwishes\Cli\Console\Infrastructure
 */
class DockerComposeInfrastructure implements Infrastructure
{
    /** @var DockerCompose\HostnameSubstitution $hostnameSubstitution */
    private $hostnameSubstitution;
    /** @var DockerCompose\ConfigFileProvider $configFileProvider */
    private $configFileProvider;
    /** @var Environment $environment */
    private $environment;
    /** @var DockerCompose\Cmd $dockerComposeCommand The docker-compose command interface */
    private $dockerComposeCommand;
    /** @var DockerCompose\HostsFileAccessor $hostsFileAccessor */
    private $hostsFileAccessor;
    /** @var DockerCompose\ContainerInfoProvider $containerInfoProvider */
    private $containerInfoProvider;

    /**
     * Creates a new instance of the DockerComposeInfrastructure class.
     *
     * @param Environment $environment
     * @param DockerCompose\Cmd $dockerComposeCommand DockerCompose command interface
     *
     */
    public function __construct(Environment $environment, DockerCompose\Cmd $dockerComposeCommand)
    {
        if (is_null($dockerComposeCommand))
        {
            throw new \InvalidArgumentException("No docker-compose command interface supplied");
        }

        $this->environment = $environment;
        $this->dockerComposeCommand = $dockerComposeCommand;

        $executorFactory = new Docker\ExecutorFactory();
        $executor = $executorFactory->getExecutor();

        $nameProvider = new Common\NameProvider($this->environment->getProjectDirectory());
        $this->hostsFileAccessor = new DockerCompose\HostsFileAccessor($nameProvider, $executor);

        $dockerCommand = new Docker\Cmd();
        $metaDataInspector = new DockerCompose\MetaDataInspector($nameProvider, $dockerCommand);
        $this->containerInfoProvider = new DockerCompose\ContainerInfoProvider($metaDataInspector);

        $this->configFileProvider = new DockerCompose\ConfigFileProvider($this->environment);
        $this->hostnameSubstitution = new DockerCompose\HostnameSubstitution($this->environment, $this->configFileProvider);
    }

    /**
     * Build and start all infrastructure components.
     */
    public function up()
    {
        // Replace hostname variable in docker-compose yml file
        $this->replaceHostnameDockerComposeYml();

        // Start the containers
        $result = $this->dockerComposeCommand->execute(array("up", "-d"));

        // Fix Reverse DNS Issue
        $this->fixHostnameInWebContainer();

        return $result;
    }

    /**
     * Build and start all infrastructure components.
     */
    public function build()
    {
        // Start the containers
        $result = $this->dockerComposeCommand->execute(array("build"));

        return $result;
    }

    /**
     * Start all infrastructure components.
     */
    public function start()
    {
        // Replace hostname variable in docker-compose yml file
        $this->replaceHostnameDockerComposeYml();

        // Start the containers
        $result = $this->dockerComposeCommand->execute(array("start"));

        // Fix Reverse DNS Issue
        $this->fixHostnameInWebContainer();

        return $result;
    }

    /**
     * Stop all infrastructure components.
     */
    public function stop()
    {
        return $this->dockerComposeCommand->execute(array("stop"));
    }

    /**
     * Pull the infrastructure.
     */
    public function pull()
    {
        return $this->dockerComposeCommand->execute(array("pull"));
    }

    /**
     * Restart all infrastructure components.
     */
    public function restart()
    {
        // Replace hostname variable in docker-compose yml file
        $this->replaceHostnameDockerComposeYml();

        // Start the containers
        $result = $this->dockerComposeCommand->execute(array("restart"));

        // Fix Reverse DNS Issue
        $this->fixHostnameInWebContainer();

        return $result;
    }

    /**
     * Remove all Container and Volums.
     */
    public function rm()
    {
        return $this->dockerComposeCommand->execute(array("rm", "-v", "--force"));
    }

    /**
     * Display the infrastructure status.
     */
    public function status()
    {
        return $this->dockerComposeCommand->execute(array("ps"));
    }

    /**
     * Hostname fix for dev and default
     */
    private function fixHostnameInWebContainer()
    {
        // get the domain name and ip address of the web container
        $webContainerType = "web";
        $ipAddress = $this->containerInfoProvider->getContainerIpAdress($webContainerType);
        $domainName = $this->containerInfoProvider->getContainerDomainname($webContainerType);

        if (empty($domainName))
        {
            $currentDockerComposeFile = $this->configFileProvider->getDockerComposeFileName();
            $workingDirectory = $this->environment->getProjectDirectory();

            // read the docker-compose yml file
            $dockerComposeFilePath = $workingDirectory . DIRECTORY_SEPARATOR . $currentDockerComposeFile;
            $yaml = new Parser();
            try
            {
                $yaml = $yaml->parse(file_get_contents($dockerComposeFilePath));
                $webContainer = $yaml[$webContainerType];

                $domainName = $webContainer['environment']['DOMAIN'];
            }
            catch (ParseException $e)
            {
                printf("Unable to parse the YAML string: %s", $e->getMessage());
            }
        }

        // add the hosts file entry to the php container
        $phpContainerType = "php"; // this is the container that get a host file entry that points to the web container
        $this->hostsFileAccessor->addHostRecord($phpContainerType, $ipAddress, $domainName);
    }

    /**
     * Replace Hostname in docker-compose yml file
     */
    private function replaceHostnameDockerComposeYml()
    {
        $this->hostnameSubstitution->substituteHostname();
    }
}
