<?php
/**

 */
namespace Euwishes\Cli\Console\Infrastructure;

use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\DockerCompose;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class StartCommand starts the infrastructure.
 * @package Euwishes\Cli\Console\Infrastructure
 */
class StartCommand extends Command
{
    const COMMAND_NAME = 'start';

    /** @var Infrastructure $instrastructure An instance of the infrastructure class */
    protected $instrastructure;

    public function __construct()
    {
        parent::__construct();

        $environment = new Environment();
        $configFileProvider = new DockerCompose\ConfigFileProvider($environment);
        $dockerComposeCommand = new DockerCompose\Cmd($environment->getProjectDirectory(), $configFileProvider);

        $this->instrastructure = new DockerComposeInfrastructure($environment, $dockerComposeCommand);
    }

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Starts the infrastructure components')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> command starts all infrastructure components.');
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->instrastructure->start();
    }
}
