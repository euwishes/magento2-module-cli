<?php
/**

 */
namespace Euwishes\Cli\Console\Infrastructure;

/**
 * Interface Infrastructure guarantees functions for controlling the project infrastructure.
 * @package Euwishes\Cli\Console\Infrastructure
 */
interface Infrastructure
{
    /**
     * Build and start all infrastructure components.
     */
    public function up();

    /**
     * Start all infrastructure components.
     */
    public function start();

    /**
     * Stop all infrastructure components.
     */
    public function stop();

    /**
     * Pull the infrastructure.
     */
    public function pull();

    /**
     * Display the infrastructure status.
     */
    public function status();

    /**
     * Restart all infrastructure components.
     */
    public function restart();

    /**
     * Remove all Container and Volums.
     */
    public function rm();

    /**
     * Build all infrastructure components.
     */
    public function build();
}
