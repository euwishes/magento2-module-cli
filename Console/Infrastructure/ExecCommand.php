<?php
/**

 */
namespace Euwishes\Cli\Console\Infrastructure;

use Euwishes\Cli\Console\Common;
use Euwishes\Cli\Console\Environment;
use Euwishes\Cli\Console\DockerCompose;
use Euwishes\Cli\Console\Docker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ExecCommand executes a command a running container.
 * @package Euwishes\Cli\Console\Infrastructure
 */
class ExecCommand extends Command
{
    const COMMAND_NAME = 'exec';

    const ARGUMENTNAME_CONTAINERTYPE = "containertype";
    const ARGUMENTNAME_ARGUMENTS = "arguments";

    /** @var Docker\ExecutorInterface $dockerExecutor A docker executor for running commands in docker containers */
    private $dockerExecutor;
    private $nameProvider;

    public function __construct()
    {
        parent::__construct();

        $environment = new Environment();
        $this->nameProvider = new Common\NameProvider($environment->getProjectDirectory());

        $executorFactory = new Docker\ExecutorFactory();
        $this->dockerExecutor = $executorFactory->getExecutor();
    }

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Execute a command in a running container.')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> executes the supplied command in the container of the given type.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENTNAME_CONTAINERTYPE,
                        InputArgument::REQUIRED,
                        'The container to run the command in (e.g. php)'
                    ),
                    new InputArgument(
                        self::ARGUMENTNAME_ARGUMENTS,
                        InputArgument::IS_ARRAY,
                        'The command and its parameters enclosed in double quotes'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $containerType = $input->getArgument(self::ARGUMENTNAME_CONTAINERTYPE);
        $arguments = $input->getArgument(self::ARGUMENTNAME_ARGUMENTS);

        $containerName = $this->nameProvider->getContainerName($containerType);

        $stdOut = "";
        $stdErr = "";
        $this->dockerExecutor->execute($containerName, implode(" ", $arguments), $stdOut, $stdErr);

        $output->write($stdOut);
    }
}
