# Magento 2 Command-Line Interface

## Available commands

### Api

- `Api:login`: Sign into a given Api instance
- `Api:logout`: Logs you out of the current Api instance.
- `Api:import:database`: Imports the latest database from the Api.
- `Api:import:media`: Imports the latest media files.